require('dotenv').config();
const express = require('express');
const authroute = require('./routes/authroute');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const { Role, sequelize } = require('./config/sequelize');
const cors = require('cors');

const app = express();

const port = process.env.PORT || 3001;

// Configuration de CORS
app.use(cors({
  origin: 'http://localhost:3000',
  credentials: true // Permet l'envoi de cookies et d'autres credentials dans les requêtes
}));

// Utilisation de body-parser pour analyser les corps de requête JSON et URL encodés
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Utilisation de cookie-parser pour analyser les cookies
app.use(cookieParser());

// Utilisation des routes d'authentification
app.use('/', authroute);

// Middleware de gestion d'erreurs
app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).send('Une erreur est survenue sur le serveur');
});

(async () => {
  try {
    await sequelize.authenticate();
    console.log('La connexion à la base de données a réussi');
    await sequelize.sync({ force: true });
    console.log('Les tables ont été synchronisées');

    await Role.bulkCreate([
      { nom: "admin" },
      { nom: "user" }
    ]);
  } catch (e) {
    console.log('Une erreur est survenue lors d\'une opération');
    console.error(e);
  }
})();

app.listen(port, function () {
  console.log(`Le serveur est prêt à écouter les requêtes sur le port ${port}`);
});
