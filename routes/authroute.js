const express=require('express')
const router=express.Router()
const {getUsers}=require('../controllers/usersController')
const {register,resetPassword,forgetPassword,logout,cookies,login}=require('../controllers/auth')
router.get('/users',getUsers)

router.post('/register',register)
router.post('/login',login)
router.get('/cookies',cookies)
router.get('/logout',logout)
router.post('/forgot-password',forgetPassword)
router.post('/reset-password',resetPassword)


module.exports=router