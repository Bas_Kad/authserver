const {User}=require('../config/sequelize')
const {errorFunction}=require('./errorFunction')
const usersController={
    getUsers:async (request,response)=> {
            try{
                 const users=await User.findAll()
                 response.status(200).json(users)
            }
            catch(error){
                errorFunction(error)
            }
    }
}

module.exports=usersController