const { errorFunction } = require('./errorFunction'); // Assurez-vous que le chemin est correct
const { User,Role } = require('../config/sequelize');
const jwt =require('jsonwebtoken')
const bcrypt = require('bcrypt');
const {cookieName,secretKey}=require('../config/auth')
const transporter=require('../mail/confignodemailer')
const  {option}=require('../mail/options')
const typemessage=require('../mail/typemessages')
const auths = {
    register: async (request, response) => {
        try {
            const { nom, email, password } = request.body;
            const error = {};
    
            const existUser = await User.findOne({
                where: { email: email }
            });
    
            if (existUser) {
                error.errorEmail = "Vous avez déjà un compte";
                return response.status(400).json(error);
            }
    
            const saltRounds = 10;
            const salt = await bcrypt.genSalt(saltRounds);
            const hashedPassword = await bcrypt.hash(password, salt);
    
            const user = await User.create({
                nom: nom,
                email: email,
                password: hashedPassword,
                refreshToken: ""
            });
    
            const role2 = await Role.findOne({
                where: {
                    nom: 'admin'
                }
            });
    
            await user.addRole(role2);
    
            const token = jwt.sign({ id: user.id, roles: ['admin'] }, secretKey, { expiresIn: "10m" });
    
            // const refreshToken = jwt.sign({ id: user.id, roles: ['user'] }, authKey().authKey);
    
            // await User.update({
            //     refreshToken: refreshToken
            // }, {
            //     where: {
            //         id: user.id
            //     }
            // });
      // Envoyer l'e-mail avec le token de réinitialisation
         await transporter.sendMail(option(email, typemessage.inscription));
    
            response.cookie(cookieName, token, {
                httpOnly: true,
                secure: true
            });

        //    await  transporter.sendMail(option(email,typemessage.inscription),function(error,info){
        //            if(error){
        //                response.status(500).json({message:"une errruer est survenue lors d'envois d'email"})
        //            }
        //            console.log('Email envoyé avec succés',info.response)
        //     })
            response.status(200).json({message:"user added with successfully"});
        } catch (error) {
            console.error('Une erreur est survenue lors de la création de l\'utilisateur :', error);
            response.status(500).json({ message: "Une erreur est survenue lors de la création de l'utilisateur." });
        }
    },
    
    login: async (request, response) => {
        try {
            const {  email, password ,remember} = request.body;
            console.log('remeber',remember)
            const errors = {};

            const existUser = await User.findOne({
                where: { email: email }
            });

            if (!existUser) {
                errors.errorEmail = "Vous dévrez créer un compte";
                return response.status(401).json(errors);
            }

            const isPasswordCorrect = await bcrypt.compare(password, existUser.password);
            if (!isPasswordCorrect) {
                errors.errorPassword = "Mot de passe incorrect";
                return response.status(401).json(errors);
            }

            const roles = await existUser.getRoles();
            const role = roles.map(el => el.dataValues.nom);

            const token = jwt.sign({ id: existUser.id, roles: role }, secretKey, { expiresIn: "10m" });

            // const options = remember ? {
            //     httpOnly: false,
            //     secure: false,
            //     maxAge: 3600000
            // } : {
            //     httpOnly: false,
            //     secure: false
            // };

            // const refreshToken = jwt.sign({ id: existUser.id, roles: role }, authKey().authKey);

            // await User.update({
            //     refreshToken: refreshToken
            // }, {
            //     where: {
            //         id: existUser.id
            //     }
            // });

            response.cookie(cookieName, token, {
                httpOnly: true,
                secure: true
            });
          
            return response.status(200).json(token);
        } catch (error) {
            console.error('Une erreur est survenue lors de la connexion de l\'utilisateur :', error);
            return response.status(500).json({ message: "Une erreur est survenue lors de la connexion de l'utilisateur." });
        }
    },
    cookies:async (request, response) => {
        try {
            const token=request.cookies.token
            response.status(200).json(token);
        } catch (error) {
            // Utilisation de la fonction errorFunction pour gérer l'erreur
            errorFunction(error);
            response.status(500).json({ message: "An error occurred while logging in." });
        }
    },
    logout: async (request, response) => {
        try {
            // Supprimer le cookie nommé 'token'
            response.clearCookie('token');
    
            response.status(200).json({ message: "Déconnexion réussie" });
        } catch (error) {
            // Gérer les erreurs avec une fonction d'erreur (errorFunction)
            errorFunction(error);
            // En cas d'erreur, répondre avec un statut 500 et un message JSON
            response.status(500).json({ message: "Une erreur s'est produite lors de la déconnexion." });
        }
    },
    forgetPassword: async (request, response) => {
        try {
            const { email } = request.body;
            const errors = {};
    
            const existUser = await User.findOne({
                where: { email: email }
            });
    
            if (!existUser) {
                errors.errorEmail = "Vous n'avez aucun compte ";
                return response.status(401).json(errors);
            }
    
            const token = jwt.sign({ id: existUser.id }, secretKey, { expiresIn: "10m" });
    
            // Mettre à jour le token de réinitialisation du mot de passe pour l'utilisateur existant
            await existUser.update({ reset_password_token : token });
    
            // Envoyer l'e-mail avec le token de réinitialisation
            await transporter.sendMail(option(email, typemessage.motdepasseoublier(token)));
    
            return response.status(200).json(token);
    
        } catch (error) {
            console.error('Une erreur est survenue lors de la connexion de l\'utilisateur :', error);
            return response.status(500).json({ message: "Une erreur est survenue lors de la connexion de l'utilisateur." });
        }
    },
    
    resetPassword :async (request, response) => {
        try {
            const { nouveauMotDePasse, token } = request.body;
    
            // Vérifiez si le token est présent dans la requête
            if (!token) {
                return response.status(400).json({ message: "Le token est manquant" });
            }
    
            // Recherchez l'utilisateur par le token
            const user = await User.findOne({  reset_password_token : token });
    
            if (!user) {
                return response.status(401).json({ message: "Le token fourni n'existe pas" });
            }
    
            // Vérifiez le jeton JWT
            jwt.verify(token, secretKey, async (error, decoded) => {
                if (error) {
                    console.error("Erreur lors de la vérification du jeton JWT :", error);
                    return response.status(403).json({ message: "Le token est invalide" });
                }
    
                // Générez le nouveau mot de passe haché
                const hashedPassword = await bcrypt.hash(nouveauMotDePasse, 10);
    
                // Mettez à jour le mot de passe de l'utilisateur
                await user.update({ password: hashedPassword });
    
                return response.status(200).json({ message: "Le mot de passe a été réinitialisé avec succès" });
            });
    
        } catch (error) {
            console.error("Une erreur est survenue lors de la réinitialisation du mot de passe :", error);
            return response.status(500).json({ message: "Une erreur est survenue lors de la réinitialisation du mot de passe" });
        }
    }
};

module.exports = auths;
