const typemessage={
    inscription:`
       <div class="container">
       <h1>Bienvenue sur G-Emploi</h1>
       <p>
          Ceci est un site web qui va vous permettre de consulter votre emploi du temps de la semaine.
       </p>
       <p>Visitez notre site: <a href="https://www.gemploi.com">www.gemploi.com</a></p>
       <p class="footer">G-Emploi - Tous droits réservés.</p>
     </div>`,
     motdepasseoublier:(token)=>{
       return `
       <div class="container">
       <h2>Réinitialisation du Mot de Passe</h2>
       <p>Cher Utilisateur,</p>
       <p>Vous avez récemment demandé une réinitialisation de votre mot de passe pour votre compte sur notre plateforme. Pour procéder à cette réinitialisation, veuillez cliquer sur le bouton ci-dessous :</p>
       <p style="text-align: center;"><a style={color:#ffffff} href="http://localhost:3000/update-password/${token}" class="btn">Réinitialiser le Mot de Passe</a></p>
       <p>Si vous n'avez pas demandé cette réinitialisation ou si vous ne souhaitez pas modifier votre mot de passe, vous pouvez ignorer cet e-mail en toute sécurité.</p>
       <p>Cordialement,<br>L'équipe de G-Emploi</p>
   </div>
        `
     } 
}

module.exports=typemessage