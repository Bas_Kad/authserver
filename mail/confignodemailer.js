const nodemailer = require('nodemailer');

const config = {
    host: "smtp.gmail.com",
    auth: {
        user: process.env. EMAIL_EMETTEUR,
        pass: process.env.PASSWORD_EMAIL
    },
    port: 587,
    secure: false, // Utilisez true pour une connexion sécurisée (TLS)
    tls: {
        rejectUnauthorized: false // Laissez-le sur true pour rejeter les certificats non autorisés
    }
};
const transporter = nodemailer.createTransport(config);
module.exports = transporter;
