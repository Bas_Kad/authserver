
const {DataTypes, Model}=require('sequelize')
function  initialModelRole(sequelize){
        class Role extends Model {}

        Role.init({
               id:{
                  type:DataTypes.INTEGER,
                  primaryKey:true,
                  autoIncrement:true,
                  allowNull:false
               },
               nom:{
                   type:DataTypes.STRING,
                   allowNull:false
               }
        },{
             sequelize,
             modelName:"Role",
             tableName:"roles"
        })

        Role.associate=({User})=>{
              Role.belongsToMany(User,{through:'UserRole'})
        }

        return Role
}

module.exports={initialModelRole}