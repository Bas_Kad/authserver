
const {DataTypes, Model}=require('sequelize')
function  initialModelUser(sequelize){
        class User extends Model {}

        User.init({
               id:{
                  type:DataTypes.INTEGER,
                  primaryKey:true,
                  autoIncrement:true,
                  allowNull:false
               },
               nom:{
                   type:DataTypes.STRING,
                   allowNull:false
               },
               email:{
                   type:DataTypes.STRING,
                   allowNull:false
               },
               password:{
                  type:DataTypes.STRING,
                  allowNull:false
               },
               refreshToken:{
                  type:DataTypes.STRING,
               },
               reset_password_token :{
                  type:DataTypes.STRING,
               }
        },{
             sequelize,
             modelName:"User",
             tableName:"users"
        })

        User.associate=({Role})=>{
              User.belongsToMany(Role,{through:'userRole'})
        }
        return User
}

module.exports={initialModelUser}