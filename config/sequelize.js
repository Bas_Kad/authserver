const { Sequelize } = require('sequelize');
const { config } = require('./config.json');
const {initialModelUser}=require('../models/User')
const {initialModelRole}=require('../models/Role')
const sequelize = new Sequelize(config.database, config.username, config.password, {
    host: config.host,
    port: config.port,
    dialect: config.dialect,
    define: config.define
});

const User=initialModelUser(sequelize)
const Role=initialModelRole(sequelize)

User.associate({Role})
Role.associate({User})

module.exports = { sequelize, User,Role };
